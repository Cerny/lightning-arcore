/*
 * Copyright 2018 Google LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.ar.sceneform.samples.hellosceneform;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.Toast;
import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.assets.RenderableSource;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;
import com.google.ar.sceneform.*;

/**
 * This is an example activity that uses the Sceneform UX package to make common AR tasks easier.
 */
public class HelloSceneformActivity extends AppCompatActivity {
  private static final String TAG = HelloSceneformActivity.class.getSimpleName();
  private static final double MIN_OPENGL_VERSION = 3.0;

  private ArFragment arFragment;
    private ModelRenderable cameraRenderable;
    private ModelRenderable shoeRenderable;
    private ModelRenderable chairRenderable;


    private static final String CAMERA_ASSET =
            "https://github.com/KhronosGroup/glTF-Sample-Models/raw/master/2.0/AntiqueCamera/glTF-Binary/AntiqueCamera.glb";
    private static final String SHOE_ASSET =
            "https://github.com/KhronosGroup/glTF-Sample-Models/raw/master/2.0/MaterialsVariantsShoe/glTF-Binary/MaterialsVariantsShoe.glb";
    private static final String CHAIR_ASSET =
            "https://gitlab.com/Cerny/lightning-arcore/-/raw/master/black_leather_chair.glb?inline=false";

  @Override
  @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
  // CompletableFuture requires api level 24
  // FutureReturnValueIgnored is not valid
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (!checkIsSupportedDeviceOrFinish(this)) {
      return;
    }

    setContentView(R.layout.activity_ux);
    arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

      ModelRenderable.builder()
              .setSource(this, RenderableSource.builder().setSource(
                      this,
                      Uri.parse(CAMERA_ASSET),
                      RenderableSource.SourceType.GLB)
                      .setScale(0.1f)  // Scale the original model to 50%.
                      .setRecenterMode(RenderableSource.RecenterMode.ROOT)
                      .build())
              .setRegistryId(CAMERA_ASSET)
              .build()
              .thenAccept(renderable -> cameraRenderable = renderable)
              .exceptionally(
                      throwable -> {
                          Toast toast =
                                  Toast.makeText(this, "Unable to load renderable " +
                                          CAMERA_ASSET, Toast.LENGTH_LONG);
                          toast.setGravity(Gravity.CENTER, 0, 0);
                          toast.show();
                          return null;
                      });

      ModelRenderable.builder()
              .setSource(this, RenderableSource.builder().setSource(
                      this,
                      Uri.parse(SHOE_ASSET),
                      RenderableSource.SourceType.GLB)
                      .setScale(1f)  // Scale the original model to 50%.
                      .setRecenterMode(RenderableSource.RecenterMode.ROOT)
                      .build())
              .setRegistryId(SHOE_ASSET)
              .build()
              .thenAccept(renderable -> shoeRenderable = renderable)
              .exceptionally(
                      throwable -> {
                          Toast toast =
                                  Toast.makeText(this, "Unable to load renderable " +
                                          SHOE_ASSET, Toast.LENGTH_LONG);
                          toast.setGravity(Gravity.CENTER, 0, 0);
                          toast.show();
                          return null;
                      });

      ModelRenderable.builder()
              .setSource(this, RenderableSource.builder().setSource(
                      this,
                      Uri.parse(CHAIR_ASSET),
                      RenderableSource.SourceType.GLB)
                      .setScale(1f)  // Scale the original model to 50%.
                      .setRecenterMode(RenderableSource.RecenterMode.ROOT)
                      .build())
              .setRegistryId(CHAIR_ASSET)
              .build()
              .thenAccept(renderable -> chairRenderable = renderable)
              .exceptionally(
                      throwable -> {
                          Toast toast =
                                  Toast.makeText(this, "Unable to load renderable " +
                                          CHAIR_ASSET, Toast.LENGTH_LONG);
                          toast.setGravity(Gravity.CENTER, 0, 0);
                          toast.show();
                          return null;
                      });





      arFragment.setOnTapArPlaneListener(
        (HitResult hitResult, Plane plane, MotionEvent motionEvent) -> {

            ModelRenderable modelToAdd = modelToAdd();
          if (modelToAdd == null) {
            return;
          }

          // Create the Anchor.
          Anchor anchor = hitResult.createAnchor();
          AnchorNode anchorNode = new AnchorNode(anchor);
          anchorNode.setParent(arFragment.getArSceneView().getScene());

          // Create the transformable andy and add it to the anchor.
          TransformableNode andy = new TransformableNode(arFragment.getTransformationSystem());
          andy.setParent(anchorNode);
          andy.setRenderable(modelToAdd);
          andy.select();
        });

  }

    ModelRenderable modelToAdd() {
      ModelRenderable modelToReturn = null;
        if (cameraRenderable != null) {
            modelToReturn = cameraRenderable;
            cameraRenderable = null;
        } else if (shoeRenderable != null) {
            modelToReturn = shoeRenderable;
            shoeRenderable = null;
        } else {
            modelToReturn = chairRenderable;
            chairRenderable = null;
        }
        return  modelToReturn;
    }



  /**
   * Returns false and displays an error message if Sceneform can not run, true if Sceneform can run
   * on this device.
   *
   * <p>Sceneform requires Android N on the device as well as OpenGL 3.0 capabilities.
   *
   * <p>Finishes the activity if Sceneform can not run
   */
  public static boolean checkIsSupportedDeviceOrFinish(final Activity activity) {
    if (Build.VERSION.SDK_INT < VERSION_CODES.N) {
      Log.e(TAG, "Sceneform requires Android N or later");
      Toast.makeText(activity, "Sceneform requires Android N or later", Toast.LENGTH_LONG).show();
      activity.finish();
      return false;
    }
    String openGlVersionString =
        ((ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE))
            .getDeviceConfigurationInfo()
            .getGlEsVersion();
    if (Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
      Log.e(TAG, "Sceneform requires OpenGL ES 3.0 later");
      Toast.makeText(activity, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
          .show();
      activity.finish();
      return false;
    }
    return true;
  }
}
